db.fruits.insertMany([
  {
    name: "Apple",
    color: "Red",
    stock: 20,
    price: 40,
    supplier_id: 1,
    onSale: true,
    origin: ["Philippines", "US"],
  },

  {
    name: "Banana",
    color: "Yellow",
    stock: 15,
    price: 20,
    supplier_id: 2,
    onSale: true,
    origin: ["Philippines", "Ecuador"],
  },

  {
    name: "Kiwi",
    color: "Green",
    stock: 25,
    price: 50,
    supplier_id: 1,
    onSale: true,
    origin: ["US", "China"],
  },

  {
    name: "Mango",
    color: "Yellow",
    stock: 10,
    price: 120,
    supplier_id: 2,
    onSale: false,
    origin: ["Philippines", "India"],
  },
]);

// mongoDB  Aggregation------------

// Aggregate method-------------
// {$match: {field:value}}---

db.fruits.aggregate([
  {
    $match: { onSale: true },
  },
]);

// {$group: {}}---
// first key-value pair is crietria kung anong pag group nila, in this case, group by same id
db.fruits.aggregate([
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
]);

// aggregating documents using 2 pipeline stages------------------
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
]);

// Field Projection with aggregation-------------
// $project---
// all the fields will be projected except for the field set to 0
db.fruits.aggregate([{ $project: { _id: 0 } }]);
// only the field that is set to 1 will be projected
db.fruits.aggregate([{ $project: { _id: 1 } }]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $project: { _id: 0 } },
]);

// Sortingwith aggregated results-------------------
// $sort----
// -1 value will make sorting reversed
// {$sort: {field:  1/-1}}
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $sort: { total: -1 } }, // can try 1
]);

// Aggregating results b ased on array fields---------------
// $unwind---
// deconstructs array field with an array value to give us a result for
// each array element.
// {$unwind: field}
db.fruits.aggregate([{ $unwind: "$origin" }]);

// display fruits documents by their origin and the kinds of fruits
// that are supplied
db.fruits.aggregate([
  { $unwind: "$origin" },
  { $group: { _id: "$origin", kinds: { $sum: 1 } } },
  { $sort: { kinds: -1 } }, // can try 1
]);
